// metalsmith and plugins
var metalsmith = require('metalsmith');
var markdown = require('metalsmith-markdown');
var layouts = require('metalsmith-layouts');
var permalinks  = require('metalsmith-permalinks');
var metallic = require('metalsmith-metallic');
var assets = require('metalsmith-assets');
var rimraf = require('rimraf');
var sass = require('metalsmith-sass');

// my plugins
var posts = require('./plugins/posts.js');

// other
var browserSync = require('browser-sync');
var argv = require('minimist')(process.argv, {boolean: true});

if(argv.deploy) {
  build(function(){});
}
else if(argv.clean) {
  console.log('Cleaning.')
  rimraf('node_modules', function(){});
  rimraf('build', function(){});
}
else{
  console.log('Usage:');
  console.log('  node index.js --deploy');
  console.log('  node index.js --clean');
  console.log('  node index.js: dev mode with hit reloading server');
  console.log('');
  browserSync({
    server: 'build',
    files: ['src/*.md', 'assets/**/*', 'layouts/**/*.njk'],
    watch: ['src/**/*', {}].concat('all', build)
  })
}

function build(callback) {
  metalsmith(__dirname)
    .metadata({
      sitename: "My Static Site & Blog",
      siteurl: "http://localhost:3000",
      description: "It's about saying »Hello« to the world.",
      generatorname: "Metalsmith",
      generatorurl: "http://metalsmith.io/"
    })
    .clean(true)
    .use(assets({
      source: "./assets",
      destination: "./assets"
    }))
    .use(metallic())
    .use(sass({
      sourceMap: true
    }))
    .use(markdown())
    .use(permalinks())
    .use(posts())
    .use(layouts())
    // .use(serve({
    //   verbose: true
    // }))
    // .use(watch({
    //   livereload: true,
    //   paths: {
    //       "${source}/**/*": true
    //   }
    // }))  
    .build(function(err) {
      var message = err ? err : 'Build complete.';
      console.log(message);
      callback();
    });
  }