---
title: "Blogging"
layout: page.njk
---

## My new blog

It's been something always lurking in the background of my mind: blogging. Several times in the past years, I've started writing blog posts. 

### Metalsmith

This blog is created using [metalsmith](http://metalsmith.io). I selected metalsmith because it's fully customizable and it comes with plugins for the most important couple of features that I need for my blog:

* Syntax highlighting
* Templates
* Markdown

<div class="hero is-primary">

</div>

```c-sharp
public class GenericPair<TFirst,TSecond>

    public TFirst First { get; private set; }
    public TSecond Second { get; private set;  }

    public GenericPair(TFirst first, TSecond second) {
        First = first;
        Second = second;
    }
]
```