---
layout: page.njk
title: Home
---

<div class="hero is-medium">
    <div class="hero-body">
        <div class="container has-text-centered">
            <p class="content">
                Hey there and welcome! My name is
            </p>
            <h1 class="subtitle">
                Tom Staijen
            </h1>
        </div>
    </div>
</div>