module.exports = posts;

function posts(options){
    return function(files, metalsmith, done) {
        setImmediate(done);
        posts = [];
        Object.keys(files).forEach(function(file) {
            var data = files[file];
            if (file.startsWith("posts")) {
                posts.push(data);
            }
            data.posts = posts;
        })
    }
}